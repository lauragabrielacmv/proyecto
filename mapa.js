let miMapa = L.map('mapid');

miMapa.setView([4.6500027,-74.170230], 16);

let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
miProveedor.addTo(miMapa);



var redIcon = L.icon({
    iconUrl: 'https://leafletjs.com/examples/custom-icons/leaf-red.png',
    shadowUrl: 'https://leafletjs.com/examples/custom-icons/leaf-shadow.png',

    iconSize:     [38, 95], 
    shadowSize:   [50, 64], 
    iconAnchor:   [22, 94],
    shadowAnchor: [4, 62],  
    popupAnchor:  [-3, -76] 
});

let miMarcador = L.marker([4.652329, -74.170547], {icon: redIcon}).addTo(miMapa);
miMarcador.addTo(miMapa)
var popup = L.popup();

miMarcador.bindPopup(" Los Frijolitos</b><br>Vigna Umbellata").openPopup();


//json:conjunto de atributos definidos dentro decorchetes (objetos)

//GeoJSON:describe objetos geograficos 
// se puedes describir puntos(point)
//lineas 
//poligono
//multiples lineas
//multiples poligonos 
//geometry collection

//objeto geografico +metadata(otros atributos no geograficos)= entidad (Features)


L.geoJSON(sitio).addTo(miMapa)


let miInfo=document.getElementById("info");
miInfo.textContent=sitio.features[0].properties.info;

let miDescripcion=document.getElementById("description");
miDescripcion.textContent=sitio.features[0].properties.description;

let miDescripcion2=document.getElementById("description2");
miDescripcion2.textContent=sitio.features[0].properties.description2;

let miTitulo=document.getElementById("tittle");
miTitulo.textContent=sitio.features[0].properties.tittle;

let miGeneral=document.getElementById("general");
miGeneral.textContent=sitio.features[0].properties.general;

let miFecha=document.getElementById("date");
miFecha.textContent=sitio.features[0].properties.date;